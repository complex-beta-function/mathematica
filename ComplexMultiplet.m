(* ::Package:: *)

BeginPackage["ComplexMultiplet`"];


Unprotect @@ Names["ComplexMultiplet`*"];
ClearAll @@ Names["ComplexMultiplet`*"];


BetaFn1l::usage = "BetaFn1l[\!\(\*SubscriptBox[\(j\), \(\[CurlyPhi]\)]\),\!\(\*SubscriptBox[\(Y\), \(\[CurlyPhi]\)]\),{g}] gives the one-loop beta functions for the Standard Model + complex scalar multiplet couplings {g} for scalar isospin \!\(\*SubscriptBox[\(j\), \(\[CurlyPhi]\)]\) and hypercharge \!\(\*SubscriptBox[\(Y\), \(\[CurlyPhi]\)]\). It is recommended to use the GenerateCouplings function to generate {g}.";
BetaFn2l::usage ="BetaFn2l[\!\(\*SubscriptBox[\(j\), \(\[CurlyPhi]\)]\),\!\(\*SubscriptBox[\(Y\), \(\[CurlyPhi]\)]\),{g}] gives the two-loop beta functions for the Standard Model + complex scalar multiplet couplings {g} for scalar isospin \!\(\*SubscriptBox[\(j\), \(\[CurlyPhi]\)]\) and hypercharge \!\(\*SubscriptBox[\(Y\), \(\[CurlyPhi]\)]\). It is recommended to use the GenerateCouplings function to generate {g}.";
GenerateCouplings::usage = "GenerateCouplings[\!\(\*SubscriptBox[\(j\), \(\[CurlyPhi]\)]\),\[Mu]] generates the scale \[Mu]-dependent set of SM + scalar multiplet couplings in the correct order to be used as inputs for BetaFn1l and BetaFn2l for scalar isospin \!\(\*SubscriptBox[\(j\), \(\[CurlyPhi]\)]\).";
GenerateCouplingNames::usage = "GenerateCouplingNames[\!\(\*SubscriptBox[\(j\), \(\[CurlyPhi]\)]\)] generates the names of the set of SM + scalar multiplet couplings for scalar isospin \!\(\*SubscriptBox[\(j\), \(\[CurlyPhi]\)]\), ideal for use in e.g. NDSolve.";
kCoefficient::usage = "kCoefficient[\!\(\*SubscriptBox[\(j\), \(\[CurlyPhi]\)]\),\!\(\*SubscriptBox[\(J\), \(1\)]\),\!\(\*SubscriptBox[\(J\), \(2\)]\),\!\(\*SubscriptBox[\(J\), \(3\)]\)] defines K(Subscript[J, 1],Subscript[J, 2],Subscript[J, 3]) for scalar isospin \!\(\*SubscriptBox[\(j\), \(\[CurlyPhi]\)]\)";
multipletDim::usage = "multipletDim[J] defines dimension of isospin J representation: \[ScriptCapitalD](J)=2J+1";
multipletJiso::usage = "multpletJiso[J] defines Cassimir of isospin J representation: \!\(\*SubscriptBox[\(\[Sum]\), \(a\)]\)\!\(\*SubscriptBox[\(\[Sum]\), \(l\)]\)(\!\(\*SuperscriptBox[\(\[Tau]\), \(\((J)\), a\)]\)\!\(\*SubscriptBox[\()\), \(il\)]\)(\!\(\*SuperscriptBox[\(\[Tau]\), \(\((J)\), a\)]\)\!\(\*SubscriptBox[\()\), \(lk\)]\)=\[ScriptCapitalJ](J)Subscript[\[Delta], ik]=J(J+1)Subscript[\[Delta], ik]";
multipletPhase::usage = "multpletPhase[j,J] defines phase factor for symmetry relations of Clebsch-Gordan Coefficients corresponding to tensor product of two isospin j representations to form an isospin J representation: Subscript[C, jj](JM;mn)=(-1)^(J-2j)Subscript[C, jj](JM;nm).";
Jsymmin::usage = "Jsymmin[j] defines lower bound for restricted sums: Subscript[\[Sum], J=0]^(2j) 1/2(1+(-1)^(J-2j))...=\[Sum]Subscript[', J=Jsymmin]^(2j) ...";
g1Coupling::usage = "Default SM U(1) coupling";
g2Coupling::usage = "Defualt SM SU(2) coupling";
gsCoupling::usage = "Default SM SU(3) coupling";
ytCoupling::usage = "Default SM top quark Yukawa";
ybCoupling::usage = "Default SM bottom quark Yukawa";
ycCoupling::usage = "Default SM charm quark Yukawa";
y\[Tau]Coupling::usage = "Default SM tau lepton Yukawa";
\[Lambda]HCoupling::usage = "Default SM Higgs doublet quartic coupling";
\[Lambda]\[CurlyPhi]H1Coupling::usage = "Default First SM + CSM Higgs portal coupling";
\[Lambda]\[CurlyPhi]H2Coupling::usage = "Default Second SM + CSM Higgs portal coupling";


Begin["`Private`"]


Print["ComplexMultiplet introduces four main functions which are used to analytically calculate the beta function for models which extend the standard model by a single complex scalar multiplet: GenerateCouplings[\!\(\*SubscriptBox[\(j\), \(\[CurlyPhi]\)]\),\[Mu]], GenerateCouplingNames[\!\(\*SubscriptBox[\(j\), \(\[CurlyPhi]\)]\)], BetaFn1l[\!\(\*SubscriptBox[\(j\), \(\[CurlyPhi]\)]\),\!\(\*SubscriptBox[\(Y\), \(\[CurlyPhi]\)]\),{g}], BetaFn2l[\!\(\*SubscriptBox[\(j\), \(\[CurlyPhi]\)]\),\!\(\*SubscriptBox[\(Y\), \(\[CurlyPhi]\)]\),{g}]. Run e.g. ?BetaFn1l for more details on individual functions."];


(* ::Title:: *)
(*Group Theory Functions*)


Clear[nineJSymbol,kCoefficient,Cfun]
	nineJSymbol[{j1_,j2_,j3_},{j4_,j5_,j6_},{j7_,j8_,j9_}] := Module[{kmin,kmax},
		kmin = Max[{Abs[j1-j9],Abs[j4-j8],Abs[j2-j6]}];
			kmax = Min[{Abs[j1+j9],Abs[j4+j8],Abs[j2+j6]}];
		Sum[(-1)^(2 k) (2 k+1) SixJSymbol[{j1,j4,j7},{j8,j9,k}]  SixJSymbol[{j2,j5,j8},{j4,k,j6}] SixJSymbol[{j3,j6,j9},{k,j1,j2}],{k,kmin,kmax}]
		] (*Wigner 9-j Symbol from https://library.wolfram.com/infocenter/MathSource/481/ by Alec Wodtke and Josh Halpern*)
kCoefficient[j_,J1_,J2_,J3_]:=(-1)^(J1+J2+J3+2j) (2J1+1)(2J2+1)nineJSymbol[{j,J2,j},{J1,j,j},{j,j,J3}] (*Defines K(Subscript[J, 1],Subscript[J, 2],Subscript[J, 3]) for scalar isospin j*)
multipletDim[J_]:=2J+1 (*Defines Dimension of isospin J representation: \[ScriptCapitalD](J)=2J+1*)
multipletJiso[J_]:=J(J+1) (*Defines Cassimir of isospin J representation: \!\(
\*SubscriptBox[\(\[Sum]\), \(a\)]\(
\*SubscriptBox[\(\[Sum]\), \(l\)]
\*SubscriptBox[\((
\*SuperscriptBox[\(\[Tau]\), \(\((J)\), a\)])\), \(il\)]
\*SubscriptBox[\((
\*SuperscriptBox[\(\[Tau]\), \(\((J)\), a\)])\), \(lk\)]\)\)=\[ScriptCapitalJ](J)Subscript[\[Delta], ik]=J(J+1)Subscript[\[Delta], ik]*)
multipletPhase[j_,J_]:=(-1)^(J-2j) (*Defines phase factor for symmetry relations of Clebsch-Gordan Coefficients corresponding to tensor product of two isospin j representations to form an isospin J representation: 
Subscript[C, jj](JM;mn)=(-1)^(J-2j)Subscript[C, jj](JM;nm)*)
Jsymmin[j_]:=If[j\[Element]Integers,0,1] (*Defines lower bound for restricted sums: Subscript[\[Sum], J=0]^(2j) 1/2(1+(-1)^(J-2j))...=\[Sum]Subscript[', J=Jsymmin]^(2j) ...*)


(* ::Title::Closed:: *)
(*1loop \[Beta]-Functions: SM + Complex Scalar with Isospin j>0, Hypercharge Y*)


BetaEquations1l[jphi_,Yph_,couplingConstants_]:=Flatten[{
(*mu g1Coupling'[mu]\[Equal]*)41./96.*couplingConstants[[1]]^3*\[Pi]^(-2)
+1./192.*multipletDim[jphi]*couplingConstants[[1]]^3*\[Pi]^(-2)*Yph^2,
(*mu g2Coupling'[mu]\[Equal]*)-19./96.*\[Pi]^(-2)*couplingConstants[[2]]^3
+1./144.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^3,
(*mu gs'[mu]\[Equal]*)-7./16.*\[Pi]^(-2)*couplingConstants[[3]]^3,
(*mu ytCouplingop'[mu]\[Equal]*)1./16.*\[Pi]^(-2)*couplingConstants[[4]]*couplingConstants[[7]]^2
+3./16.*\[Pi]^(-2)*couplingConstants[[4]]*couplingConstants[[6]]^2
+3./32.*\[Pi]^(-2)*couplingConstants[[4]]*couplingConstants[[5]]^2
+9./32.*\[Pi]^(-2)*couplingConstants[[4]]^3
-1./2.*\[Pi]^(-2)*couplingConstants[[3]]^2*couplingConstants[[4]]
-9./64.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[4]]
-17./192.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[4]],
(*mu ybCouplingot'[mu]\[Equal]*)1./16.*\[Pi]^(-2)*couplingConstants[[5]]*couplingConstants[[7]]^2
+3./16.*\[Pi]^(-2)*couplingConstants[[5]]*couplingConstants[[6]]^2
+9./32.*\[Pi]^(-2)*couplingConstants[[5]]^3
+3./32.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[5]]
-1./2.*\[Pi]^(-2)*couplingConstants[[3]]^2*couplingConstants[[5]]
-9./64.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[5]]
-5./192.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[5]],
(*mu ycCouplingha'[mu]\[Equal]*)1./16.*\[Pi]^(-2)*couplingConstants[[6]]*couplingConstants[[7]]^2
+9./32.*\[Pi]^(-2)*couplingConstants[[6]]^3
+3./16.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[6]]
+3./16.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[6]]
-1./2.*\[Pi]^(-2)*couplingConstants[[3]]^2*couplingConstants[[6]]
-9./64.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[6]]
-17./192.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[6]],
(*mu ytCouplingau'[mu]\[Equal]*)5./32.*\[Pi]^(-2)*couplingConstants[[7]]^3
+3./16.*\[Pi]^(-2)*couplingConstants[[6]]^2*couplingConstants[[7]]
+3./16.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[7]]
+3./16.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[7]]
-9./64.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[7]]
-15./64.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[7]],
(*mu lam'[mu]\[Equal]*)3./8.*\[Pi]^(-2)*couplingConstants[[8]]^2
+1./4.*\[Pi]^(-2)*couplingConstants[[7]]^2*couplingConstants[[8]]
-1./2.*\[Pi]^(-2)*couplingConstants[[7]]^4
+3./4.*\[Pi]^(-2)*couplingConstants[[6]]^2*couplingConstants[[8]]
-3./2.*\[Pi]^(-2)*couplingConstants[[6]]^4
+3./4.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[8]]
-3./2.*\[Pi]^(-2)*couplingConstants[[5]]^4
+3./4.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[8]]
-3./2.*\[Pi]^(-2)*couplingConstants[[4]]^4
-9./16.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[8]]
+9./32.*\[Pi]^(-2)*couplingConstants[[2]]^4
-3./16.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[8]]
+3./16.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[2]]^2
+3./32.*couplingConstants[[1]]^4*\[Pi]^(-2)
+1./64.*multipletDim[jphi]*\[Pi]^(-2)*couplingConstants[[9]]^2
+1./768.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[10]]^2,
(*mu l1phH'[mu]\[Equal]*)1./16.*\[Pi]^(-2)*couplingConstants[[9]]^2
+3./16.*\[Pi]^(-2)*couplingConstants[[8]]*couplingConstants[[9]]
+1./8.*\[Pi]^(-2)*couplingConstants[[7]]^2*couplingConstants[[9]]
+3./8.*\[Pi]^(-2)*couplingConstants[[6]]^2*couplingConstants[[9]]
+3./8.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[9]]
+3./8.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[9]]
-9./32.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[9]]
-3./32.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[9]]
-3./32.*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2*couplingConstants[[9]]
+3./16.*couplingConstants[[1]]^4*\[Pi]^(-2)*Yph^2
+1./64.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[10]]^2
-3./8.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[9]]
+3./4.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^4
+Sum[1./8./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-2)*couplingConstants[[9]],{J1,Jsymmin[jphi],2jphi,2}],
(*mu l2phH'[mu]\[Equal]*)1./8.*\[Pi]^(-2)*couplingConstants[[9]]*couplingConstants[[10]]
+1./16.*\[Pi]^(-2)*couplingConstants[[8]]*couplingConstants[[10]]
+1./8.*\[Pi]^(-2)*couplingConstants[[7]]^2*couplingConstants[[10]]
+3./8.*\[Pi]^(-2)*couplingConstants[[6]]^2*couplingConstants[[10]]
+3./8.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[10]]
+3./8.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[10]]
-9./32.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[10]]
-3./32.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[10]]
+3./2.*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph*couplingConstants[[2]]^2
-3./32.*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2*couplingConstants[[10]]
-3./8.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[10]]
+Sum[-1./8./(multipletDim[jphi]*multipletJiso[jphi])*multipletDim[J1]*multipletJiso[jphi]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-2)*couplingConstants[[10]]
+1./16./(multipletDim[jphi]*multipletJiso[jphi])*multipletDim[J1]*multipletJiso[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-2)*couplingConstants[[10]],{J1,Jsymmin[jphi],2jphi,2}],
Table[(*mu couplingConstants[J]'[mu]\[Equal]*)1./32.*\[Pi]^(-2)*couplingConstants[[9]]^2
+3./32.*couplingConstants[[1]]^4*\[Pi]^(-2)*Yph^4
-1./128.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[10]]^2
-3./4.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^4
-3./4.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2*couplingConstants[[2]]^2
+3./2.*multipletJiso[jphi]^2*\[Pi]^(-2)*couplingConstants[[2]]^4
-3./4.*multipletJiso[jphi]*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-2)*couplingConstants[[2]]^2
+1./256.*multipletJiso[J]*\[Pi]^(-2)*couplingConstants[[10]]^2
+3./8.*multipletJiso[J]*\[Pi]^(-2)*couplingConstants[[2]]^4
+3./8.*multipletJiso[J]*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2*couplingConstants[[2]]^2
-3./2.*multipletJiso[J]*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^4
+3./8.*multipletJiso[J]^2*\[Pi]^(-2)*couplingConstants[[2]]^4
-3./16.*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2
+1./16.*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]^2*\[Pi]^(-2)
+Sum[1./4.*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[11+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*\[Pi]^(-2),{J1,Jsymmin[jphi],2jphi,2},{J2,Jsymmin[jphi],2jphi,2}],{J,Jsymmin[jphi],2jphi,2}]}]


(* ::Title::Closed:: *)
(*1loop \[Beta]-Functions: SM + Complex Singlet Scalar, Hypercharge Y*)


BetaSingletEquations1l[jphi_,Yph_,couplingConstants_]:=Flatten[{
(*mu g1Coupling'[mu]\[Equal]*)41./96.*couplingConstants[[1]]^3*\[Pi]^(-2)
+1./192.*multipletDim[jphi]*couplingConstants[[1]]^3*\[Pi]^(-2)*Yph^2,
(*mu g2Coupling'[mu]\[Equal]*)-19./96.*\[Pi]^(-2)*couplingConstants[[2]]^3
+1./144.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^3,
(*mu gs'[mu]\[Equal]*)-7./16.*\[Pi]^(-2)*couplingConstants[[3]]^3,
(*mu ytCouplingop'[mu]\[Equal]*)1./16.*\[Pi]^(-2)*couplingConstants[[4]]*couplingConstants[[7]]^2
+3./16.*\[Pi]^(-2)*couplingConstants[[4]]*couplingConstants[[6]]^2
+3./32.*\[Pi]^(-2)*couplingConstants[[4]]*couplingConstants[[5]]^2
+9./32.*\[Pi]^(-2)*couplingConstants[[4]]^3
-1./2.*\[Pi]^(-2)*couplingConstants[[3]]^2*couplingConstants[[4]]
-9./64.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[4]]
-17./192.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[4]],
(*mu ybCouplingot'[mu]\[Equal]*)1./16.*\[Pi]^(-2)*couplingConstants[[5]]*couplingConstants[[7]]^2
+3./16.*\[Pi]^(-2)*couplingConstants[[5]]*couplingConstants[[6]]^2
+9./32.*\[Pi]^(-2)*couplingConstants[[5]]^3
+3./32.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[5]]
-1./2.*\[Pi]^(-2)*couplingConstants[[3]]^2*couplingConstants[[5]]
-9./64.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[5]]
-5./192.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[5]],
(*mu ycCouplingha'[mu]\[Equal]*)1./16.*\[Pi]^(-2)*couplingConstants[[6]]*couplingConstants[[7]]^2
+9./32.*\[Pi]^(-2)*couplingConstants[[6]]^3
+3./16.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[6]]
+3./16.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[6]]
-1./2.*\[Pi]^(-2)*couplingConstants[[3]]^2*couplingConstants[[6]]
-9./64.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[6]]
-17./192.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[6]],
(*mu ytCouplingau'[mu]\[Equal]*)5./32.*\[Pi]^(-2)*couplingConstants[[7]]^3
+3./16.*\[Pi]^(-2)*couplingConstants[[6]]^2*couplingConstants[[7]]
+3./16.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[7]]
+3./16.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[7]]
-9./64.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[7]]
-15./64.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[7]],
(*mu lam'[mu]\[Equal]*)3./8.*\[Pi]^(-2)*couplingConstants[[8]]^2
+1./4.*\[Pi]^(-2)*couplingConstants[[7]]^2*couplingConstants[[8]]
-1./2.*\[Pi]^(-2)*couplingConstants[[7]]^4
+3./4.*\[Pi]^(-2)*couplingConstants[[6]]^2*couplingConstants[[8]]
-3./2.*\[Pi]^(-2)*couplingConstants[[6]]^4
+3./4.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[8]]
-3./2.*\[Pi]^(-2)*couplingConstants[[5]]^4
+3./4.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[8]]
-3./2.*\[Pi]^(-2)*couplingConstants[[4]]^4
-9./16.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[8]]
+9./32.*\[Pi]^(-2)*couplingConstants[[2]]^4
-3./16.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[8]]
+3./16.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[2]]^2
+3./32.*couplingConstants[[1]]^4*\[Pi]^(-2)
+1./64.*multipletDim[jphi]*\[Pi]^(-2)*couplingConstants[[9]]^2,
(*mu l1phH'[mu]\[Equal]*)1./16.*\[Pi]^(-2)*couplingConstants[[9]]^2
+3./16.*\[Pi]^(-2)*couplingConstants[[8]]*couplingConstants[[9]]
+1./8.*\[Pi]^(-2)*couplingConstants[[7]]^2*couplingConstants[[9]]
+3./8.*\[Pi]^(-2)*couplingConstants[[6]]^2*couplingConstants[[9]]
+3./8.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[9]]
+3./8.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[9]]
-9./32.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[9]]
-3./32.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[9]]
-3./32.*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2*couplingConstants[[9]]
+3./16.*couplingConstants[[1]]^4*\[Pi]^(-2)*Yph^2
-3./8.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[9]]
+3./4.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^4
+Sum[1./8./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-2)*couplingConstants[[9]],{J1,Jsymmin[jphi],2jphi,2}],
Table[(*mu couplingConstants[J]'[mu]\[Equal]*)1./32.*\[Pi]^(-2)*couplingConstants[[9]]^2
+3./32.*couplingConstants[[1]]^4*\[Pi]^(-2)*Yph^4
-3./4.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^4
-3./4.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2*couplingConstants[[2]]^2
+3./2.*multipletJiso[jphi]^2*\[Pi]^(-2)*couplingConstants[[2]]^4
-3./4.*multipletJiso[jphi]*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-2)*couplingConstants[[2]]^2
+3./8.*multipletJiso[J]*\[Pi]^(-2)*couplingConstants[[2]]^4
+3./8.*multipletJiso[J]*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2*couplingConstants[[2]]^2
-3./2.*multipletJiso[J]*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^4
+3./8.*multipletJiso[J]^2*\[Pi]^(-2)*couplingConstants[[2]]^4
-3./16.*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2
+1./16.*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]^2*\[Pi]^(-2)
+Sum[1./4.*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[10+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*\[Pi]^(-2),{J1,Jsymmin[jphi],2jphi,2},{J2,Jsymmin[jphi],2jphi,2}],{J,Jsymmin[jphi],2jphi,2}]}]


(* ::Title::Closed:: *)
(*2loop \[Beta]-Functions: SM + Complex Scalar with Isospin j>0, Hypercharge Y*)


BetaEquations2l[jphi_,Yph_,couplingConstants_]:=Flatten[{
(*mu g1Coupling'[mu]\[Equal]*)-5./512.*couplingConstants[[1]]^3*\[Pi]^(-4)*couplingConstants[[7]]^2
-17./1536.*couplingConstants[[1]]^3*\[Pi]^(-4)*couplingConstants[[6]]^2
-5./1536.*couplingConstants[[1]]^3*\[Pi]^(-4)*couplingConstants[[5]]^2
-17./1536.*couplingConstants[[1]]^3*\[Pi]^(-4)*couplingConstants[[4]]^2
+11./192.*couplingConstants[[1]]^3*\[Pi]^(-4)*couplingConstants[[3]]^2
+9./512.*couplingConstants[[1]]^3*\[Pi]^(-4)*couplingConstants[[2]]^2
+41./96.*couplingConstants[[1]]^3*\[Pi]^(-2)
+199./4608.*couplingConstants[[1]]^5*\[Pi]^(-4)
+1./192.*multipletDim[jphi]*couplingConstants[[1]]^3*\[Pi]^(-2)*Yph^2
+1/(16*\[Pi]^2)^2*(1./4.*multipletDim[jphi]*couplingConstants[[1]]^5*Yph^4
+multipletDim[jphi]*multipletJiso[jphi]*couplingConstants[[1]]^3*Yph^2*couplingConstants[[2]]^2),
(*mu g2Coupling'[mu]\[Equal]*)-1./512.*\[Pi]^(-4)*couplingConstants[[2]]^3*couplingConstants[[7]]^2
-3./512.*\[Pi]^(-4)*couplingConstants[[2]]^3*couplingConstants[[6]]^2
-3./512.*\[Pi]^(-4)*couplingConstants[[2]]^3*couplingConstants[[5]]^2
-3./512.*\[Pi]^(-4)*couplingConstants[[2]]^3*couplingConstants[[4]]^2
+3./64.*\[Pi]^(-4)*couplingConstants[[2]]^3*couplingConstants[[3]]^2
+35./1536.*\[Pi]^(-4)*couplingConstants[[2]]^5
-19./96.*\[Pi]^(-2)*couplingConstants[[2]]^3
+3./512.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^3
+1./144.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^3
+1/(16*\[Pi]^2)^2*(4./9.*multipletDim[jphi]*multipletJiso[jphi]*couplingConstants[[2]]^5
+1./3.*multipletDim[jphi]*multipletJiso[jphi]*couplingConstants[[1]]^2*Yph^2*couplingConstants[[2]]^3
+4./3.*multipletDim[jphi]*multipletJiso[jphi]^2*couplingConstants[[2]]^5),
(*mu gs'[mu]\[Equal]*)-1./128.*\[Pi]^(-4)*couplingConstants[[3]]^3*couplingConstants[[6]]^2
-1./128.*\[Pi]^(-4)*couplingConstants[[3]]^3*couplingConstants[[5]]^2
-1./128.*\[Pi]^(-4)*couplingConstants[[3]]^3*couplingConstants[[4]]^2
-13./128.*\[Pi]^(-4)*couplingConstants[[3]]^5
+9./512.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[3]]^3
-7./16.*\[Pi]^(-2)*couplingConstants[[3]]^3
+11./1536.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[3]]^3,
(*mu ytCouplingop'[mu]\[Equal]*)3./2048.*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[8]]^2
-9./1024.*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[7]]^4
-27./1024.*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[6]]^4
+5./1024.*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[5]]^2*couplingConstants[[7]]^2
+15./1024.*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[5]]^2*couplingConstants[[6]]^2
-1./1024.*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[5]]^4
-3./256.*\[Pi]^(-4)*couplingConstants[[4]]^3*couplingConstants[[8]]
-9./1024.*\[Pi]^(-4)*couplingConstants[[4]]^3*couplingConstants[[7]]^2
-27./1024.*\[Pi]^(-4)*couplingConstants[[4]]^3*couplingConstants[[6]]^2
-11./1024.*\[Pi]^(-4)*couplingConstants[[4]]^3*couplingConstants[[5]]^2
-3./64.*\[Pi]^(-4)*couplingConstants[[4]]^5
+5./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]*couplingConstants[[6]]^2
+1./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]*couplingConstants[[5]]^2
+9./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]^3
-27./64.*\[Pi]^(-4)*couplingConstants[[3]]^4*couplingConstants[[4]]
+15./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]*couplingConstants[[7]]^2
+45./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]*couplingConstants[[6]]^2
+99./4096.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]*couplingConstants[[5]]^2
+225./4096.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]^3
+9./256.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[3]]^2*couplingConstants[[4]]
-23./1024.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[4]]
+1./16.*\[Pi]^(-2)*couplingConstants[[4]]*couplingConstants[[7]]^2
+3./16.*\[Pi]^(-2)*couplingConstants[[4]]*couplingConstants[[6]]^2
+3./32.*\[Pi]^(-2)*couplingConstants[[4]]*couplingConstants[[5]]^2
+9./32.*\[Pi]^(-2)*couplingConstants[[4]]^3
-1./2.*\[Pi]^(-2)*couplingConstants[[3]]^2*couplingConstants[[4]]
-9./64.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[4]]
+25./2048.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[7]]^2
+85./6144.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[6]]^2
+7./12288.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[5]]^2
+131./4096.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]^3
+19./2304.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]
-3./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]
-17./192.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[4]]
+1187./55296.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[4]]
+1./8192.*multipletDim[jphi]*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[9]]^2
+5./6912.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[4]]
+1./32768.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[10]]^2
+1./768.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[4]],
(*mu ybCouplingot'[mu]\[Equal]*)3./2048.*\[Pi]^(-4)*couplingConstants[[5]]*couplingConstants[[8]]^2
-9./1024.*\[Pi]^(-4)*couplingConstants[[5]]*couplingConstants[[7]]^4
-27./1024.*\[Pi]^(-4)*couplingConstants[[5]]*couplingConstants[[6]]^4
-3./256.*\[Pi]^(-4)*couplingConstants[[5]]^3*couplingConstants[[8]]
-9./1024.*\[Pi]^(-4)*couplingConstants[[5]]^3*couplingConstants[[7]]^2
-27./1024.*\[Pi]^(-4)*couplingConstants[[5]]^3*couplingConstants[[6]]^2
-3./64.*\[Pi]^(-4)*couplingConstants[[5]]^5
+5./1024.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]*couplingConstants[[7]]^2
+15./1024.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]*couplingConstants[[6]]^2
-11./1024.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]^3
-1./1024.*\[Pi]^(-4)*couplingConstants[[4]]^4*couplingConstants[[5]]
+5./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]*couplingConstants[[6]]^2
+9./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]^3
+1./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]^2*couplingConstants[[5]]
-27./64.*\[Pi]^(-4)*couplingConstants[[3]]^4*couplingConstants[[5]]
+15./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]*couplingConstants[[7]]^2
+45./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]*couplingConstants[[6]]^2
+225./4096.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]^3
+99./4096.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]^2*couplingConstants[[5]]
+9./256.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[3]]^2*couplingConstants[[5]]
-23./1024.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[5]]
+1./16.*\[Pi]^(-2)*couplingConstants[[5]]*couplingConstants[[7]]^2
+3./16.*\[Pi]^(-2)*couplingConstants[[5]]*couplingConstants[[6]]^2
+9./32.*\[Pi]^(-2)*couplingConstants[[5]]^3
+3./32.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[5]]
-1./2.*\[Pi]^(-2)*couplingConstants[[3]]^2*couplingConstants[[5]]
-9./64.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[5]]
+25./2048.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]*couplingConstants[[7]]^2
+85./6144.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]*couplingConstants[[6]]^2
+79./4096.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]^3
+91./12288.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]
+31./2304.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]
-9./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]
-5./192.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[5]]
-127./55296.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[5]]
+1./8192.*multipletDim[jphi]*\[Pi]^(-4)*couplingConstants[[5]]*couplingConstants[[9]]^2
+7./55296.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[5]]
+1./32768.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[5]]*couplingConstants[[10]]^2
+1./768.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[5]],
(*mu ycCouplingha'[mu]\[Equal]*)3./2048.*\[Pi]^(-4)*couplingConstants[[6]]*couplingConstants[[8]]^2
-9./1024.*\[Pi]^(-4)*couplingConstants[[6]]*couplingConstants[[7]]^4
-3./256.*\[Pi]^(-4)*couplingConstants[[6]]^3*couplingConstants[[8]]
-9./1024.*\[Pi]^(-4)*couplingConstants[[6]]^3*couplingConstants[[7]]^2
-3./64.*\[Pi]^(-4)*couplingConstants[[6]]^5
-27./1024.*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[6]]^3
-27./1024.*\[Pi]^(-4)*couplingConstants[[5]]^4*couplingConstants[[6]]
-27./1024.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[6]]^3
+3./512.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]^2*couplingConstants[[6]]
-27./1024.*\[Pi]^(-4)*couplingConstants[[4]]^4*couplingConstants[[6]]
+9./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[6]]^3
+5./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]^2*couplingConstants[[6]]
+5./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]^2*couplingConstants[[6]]
-27./64.*\[Pi]^(-4)*couplingConstants[[3]]^4*couplingConstants[[6]]
+15./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[6]]*couplingConstants[[7]]^2
+225./4096.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[6]]^3
+45./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]^2*couplingConstants[[6]]
+45./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]^2*couplingConstants[[6]]
+9./256.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[3]]^2*couplingConstants[[6]]
-23./1024.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[6]]
+1./16.*\[Pi]^(-2)*couplingConstants[[6]]*couplingConstants[[7]]^2
+9./32.*\[Pi]^(-2)*couplingConstants[[6]]^3
+3./16.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[6]]
+3./16.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[6]]
-1./2.*\[Pi]^(-2)*couplingConstants[[3]]^2*couplingConstants[[6]]
-9./64.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[6]]
+25./2048.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[6]]*couplingConstants[[7]]^2
+131./4096.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[6]]^3
+25./6144.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[6]]
+85./6144.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[6]]
+19./2304.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[6]]
-3./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[6]]
-17./192.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[6]]
+1187./55296.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[6]]
+1./8192.*multipletDim[jphi]*\[Pi]^(-4)*couplingConstants[[6]]*couplingConstants[[9]]^2
+5./6912.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[6]]
+1./32768.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[6]]*couplingConstants[[10]]^2
+1./768.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[6]],
(*mu ytCouplingau'[mu]\[Equal]*)3./2048.*\[Pi]^(-4)*couplingConstants[[7]]*couplingConstants[[8]]^2
-3./256.*\[Pi]^(-4)*couplingConstants[[7]]^3*couplingConstants[[8]]
-3./256.*\[Pi]^(-4)*couplingConstants[[7]]^5
-27./1024.*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[7]]^3
-27./1024.*\[Pi]^(-4)*couplingConstants[[6]]^4*couplingConstants[[7]]
-27./1024.*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[7]]^3
-27./1024.*\[Pi]^(-4)*couplingConstants[[5]]^4*couplingConstants[[7]]
-27./1024.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[7]]^3
+3./512.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]^2*couplingConstants[[7]]
-27./1024.*\[Pi]^(-4)*couplingConstants[[4]]^4*couplingConstants[[7]]
+5./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[6]]^2*couplingConstants[[7]]
+5./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]^2*couplingConstants[[7]]
+5./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]^2*couplingConstants[[7]]
+165./4096.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[7]]^3
+45./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[6]]^2*couplingConstants[[7]]
+45./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]^2*couplingConstants[[7]]
+45./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]^2*couplingConstants[[7]]
-23./1024.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[7]]
+5./32.*\[Pi]^(-2)*couplingConstants[[7]]^3
+3./16.*\[Pi]^(-2)*couplingConstants[[6]]^2*couplingConstants[[7]]
+3./16.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[7]]
+3./16.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[7]]
-9./64.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[7]]
+179./4096.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[7]]^3
+85./6144.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[7]]
+25./6144.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[7]]
+85./6144.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[7]]
+9./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[7]]
-15./64.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[7]]
+457./6144.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[7]]
+1./8192.*multipletDim[jphi]*\[Pi]^(-4)*couplingConstants[[7]]*couplingConstants[[9]]^2
+13./6144.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[7]]
+1./32768.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[7]]*couplingConstants[[10]]^2
+1./768.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[7]],
(*mu lam'[mu]\[Equal]*)-39./512.*\[Pi]^(-4)*couplingConstants[[8]]^3
-3./64.*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[8]]^2
-1./256.*\[Pi]^(-4)*couplingConstants[[7]]^4*couplingConstants[[8]]
+5./32.*\[Pi]^(-4)*couplingConstants[[7]]^6
-9./64.*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[8]]^2
-3./256.*\[Pi]^(-4)*couplingConstants[[6]]^4*couplingConstants[[8]]
+15./32.*\[Pi]^(-4)*couplingConstants[[6]]^6
-9./64.*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[8]]^2
-3./256.*\[Pi]^(-4)*couplingConstants[[5]]^4*couplingConstants[[8]]
+15./32.*\[Pi]^(-4)*couplingConstants[[5]]^6
-9./64.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[8]]^2
-21./128.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]^2*couplingConstants[[8]]
-3./32.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]^4
-3./256.*\[Pi]^(-4)*couplingConstants[[4]]^4*couplingConstants[[8]]
-3./32.*\[Pi]^(-4)*couplingConstants[[4]]^4*couplingConstants[[5]]^2
+15./32.*\[Pi]^(-4)*couplingConstants[[4]]^6
+5./16.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[6]]^2*couplingConstants[[8]]
-1./2.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[6]]^4
+5./16.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]^2*couplingConstants[[8]]
-1./2.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]^4
+5./16.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]^2*couplingConstants[[8]]
-1./2.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]^4
+27./256.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[8]]^2
+15./512.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[7]]^2*couplingConstants[[8]]
+45./512.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[6]]^2*couplingConstants[[8]]
+45./512.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]^2*couplingConstants[[8]]
+45./512.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]^2*couplingConstants[[8]]
-73./2048.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[8]]
-3./256.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[7]]^2
-9./256.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[6]]^2
-9./256.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[5]]^2
-9./256.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[4]]^2
+305./1024.*\[Pi]^(-4)*couplingConstants[[2]]^6
+3./8.*\[Pi]^(-2)*couplingConstants[[8]]^2
+1./4.*\[Pi]^(-2)*couplingConstants[[7]]^2*couplingConstants[[8]]
-1./2.*\[Pi]^(-2)*couplingConstants[[7]]^4
+3./4.*\[Pi]^(-2)*couplingConstants[[6]]^2*couplingConstants[[8]]
-3./2.*\[Pi]^(-2)*couplingConstants[[6]]^4
+3./4.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[8]]
-3./2.*\[Pi]^(-2)*couplingConstants[[5]]^4
+3./4.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[8]]
-3./2.*\[Pi]^(-2)*couplingConstants[[4]]^4
-9./16.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[8]]
+9./32.*\[Pi]^(-2)*couplingConstants[[2]]^4
+9./256.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[8]]^2
+25./512.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[8]]
-1./16.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[7]]^4
+85./1536.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[8]]
-1./24.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[6]]^4
+25./1536.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[8]]
+1./48.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]^4
+85./1536.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[8]]
-1./24.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]^4
+39./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[8]]
+11./128.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[7]]^2
+21./128.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[6]]^2
+9./128.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]^2
+21./128.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]^2
-289./3072.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^4
-3./16.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[8]]
+3./16.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[2]]^2
+629./6144.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[8]]
-25./256.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[7]]^2
-19./256.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[6]]^2
+5./256.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[5]]^2
-19./256.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[4]]^2
-559./3072.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[2]]^2
+3./32.*couplingConstants[[1]]^4*\[Pi]^(-2)
-379./3072.*couplingConstants[[1]]^6*\[Pi]^(-4)
-1./1024.*multipletDim[jphi]*\[Pi]^(-4)*couplingConstants[[9]]^3
-5./2048.*multipletDim[jphi]*\[Pi]^(-4)*couplingConstants[[8]]*couplingConstants[[9]]^2
+1./64.*multipletDim[jphi]*\[Pi]^(-2)*couplingConstants[[9]]^2
+1./512.*multipletDim[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[9]]^2
+5./1024.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[9]]
+11./6144.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[8]]
-7./3072.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
-7./3072.*multipletDim[jphi]*couplingConstants[[1]]^6*\[Pi]^(-4)*Yph^2
-5./12288.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[9]]*couplingConstants[[10]]^2
-7./24576.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[8]]*couplingConstants[[10]]^2
-1./2048.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[10]]^2
+1./128.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[9]]^2
+5./256.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[9]]
+11./1536.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[8]]
-7./768.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^6
+1./768.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[10]]^2
-7./2304.*multipletDim[jphi]*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^4
+5./1536.*multipletDim[jphi]*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph*couplingConstants[[2]]^2*couplingConstants[[10]]
+1./6144.*multipletDim[jphi]*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[10]]^2
+1./1536.*multipletDim[jphi]*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[10]]^2,
(*mu l1phH'[mu]\[Equal]*)-5./2048.*\[Pi]^(-4)*couplingConstants[[9]]^3
-9./512.*\[Pi]^(-4)*couplingConstants[[8]]*couplingConstants[[9]]^2
-15./1024.*\[Pi]^(-4)*couplingConstants[[8]]^2*couplingConstants[[9]]
-1./256.*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[9]]^2
-3./128.*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[8]]*couplingConstants[[9]]
-9./512.*\[Pi]^(-4)*couplingConstants[[7]]^4*couplingConstants[[9]]
-3./256.*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[9]]^2
-9./128.*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[8]]*couplingConstants[[9]]
-27./512.*\[Pi]^(-4)*couplingConstants[[6]]^4*couplingConstants[[9]]
-3./256.*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[9]]^2
-9./128.*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[8]]*couplingConstants[[9]]
-27./512.*\[Pi]^(-4)*couplingConstants[[5]]^4*couplingConstants[[9]]
-3./256.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[9]]^2
-9./128.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[8]]*couplingConstants[[9]]
-21./256.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]^2*couplingConstants[[9]]
-27./512.*\[Pi]^(-4)*couplingConstants[[4]]^4*couplingConstants[[9]]
+5./32.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[6]]^2*couplingConstants[[9]]
+5./32.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]^2*couplingConstants[[9]]
+5./32.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]^2*couplingConstants[[9]]
+3./1024.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[9]]^2
+9./128.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[8]]*couplingConstants[[9]]
+15./1024.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[7]]^2*couplingConstants[[9]]
+45./1024.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[6]]^2*couplingConstants[[9]]
+45./1024.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]^2*couplingConstants[[9]]
+45./1024.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]^2*couplingConstants[[9]]
-145./4096.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[9]]
+1./16.*\[Pi]^(-2)*couplingConstants[[9]]^2
+3./16.*\[Pi]^(-2)*couplingConstants[[8]]*couplingConstants[[9]]
+1./8.*\[Pi]^(-2)*couplingConstants[[7]]^2*couplingConstants[[9]]
+3./8.*\[Pi]^(-2)*couplingConstants[[6]]^2*couplingConstants[[9]]
+3./8.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[9]]
+3./8.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[9]]
-9./32.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[9]]
+1./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[9]]^2
+3./128.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[8]]*couplingConstants[[9]]
+25./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[9]]
+85./3072.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[9]]
+25./3072.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[9]]
+85./3072.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[9]]
+15./2048.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[9]]
+1./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[9]]^2
-3./32.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[9]]
-3./32.*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2*couplingConstants[[9]]
+557./12288.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[9]]
+223./6144.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[9]]
+15./512.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[8]]
-25./256.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[7]]^2
-19./256.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[6]]^2
+5./256.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[5]]^2
-19./256.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[4]]^2
-45./1024.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
+5./4096.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4*couplingConstants[[9]]
+3./16.*couplingConstants[[1]]^4*\[Pi]^(-2)*Yph^2
-713./3072.*couplingConstants[[1]]^6*\[Pi]^(-4)*Yph^2
-15./1024.*couplingConstants[[1]]^6*\[Pi]^(-4)*Yph^4
-1./4096.*multipletDim[jphi]*\[Pi]^(-4)*couplingConstants[[9]]^3
+11./12288.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[9]]
+71./12288.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4*couplingConstants[[9]]
-7./1536.*multipletDim[jphi]*couplingConstants[[1]]^6*\[Pi]^(-4)*Yph^4
-1./16384.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[9]]*couplingConstants[[10]]^2
+11./3072.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[9]]
+71./2304.*multipletDim[jphi]*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[9]]
-7./288.*multipletDim[jphi]*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^6
-13./8192.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[9]]*couplingConstants[[10]]^2
-5./2048.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[8]]*couplingConstants[[10]]^2
-1./1024.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[10]]^2
-3./1024.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[10]]^2
-3./1024.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[10]]^2
-3./1024.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[10]]^2
+15./4096.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[10]]^2
+1./256.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[9]]^2
-143./1536.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[9]]
+15./128.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[8]]
-1./64.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[7]]^2
-3./64.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[6]]^2
-3./64.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[5]]^2
-3./64.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[4]]^2
+745./768.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^6
+1./64.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[10]]^2
-3./8.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[9]]
+3./4.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^4
+1./4096.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[10]]^2
-15./256.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^4
+1./256.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph*couplingConstants[[2]]^2*couplingConstants[[10]]
+1./4096.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[10]]^2
+5./512.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2*couplingConstants[[9]]
-15./256.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^4
-15./256.*multipletJiso[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
+1./1024.*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[10]]^2
+5./256.*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[9]]
-15./64.*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^6
+Sum[1./1024./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[jphi]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[10]]^2
+1./16./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[jphi]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[9]]
+5./64./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[jphi]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
-1./1024./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[10]]^2
-3./256./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[9]]^2
+1./8./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-2)*couplingConstants[[9]]
+1./64./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[9]]
+5./256./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2
-5./512./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]^2*\[Pi]^(-4)*couplingConstants[[9]],{J1,Jsymmin[jphi],2jphi,2}],
(*mu l2phH'[mu]\[Equal]*)3./4096.*\[Pi]^(-4)*couplingConstants[[10]]^3
-13./2048.*\[Pi]^(-4)*couplingConstants[[9]]^2*couplingConstants[[10]]
-5./256.*\[Pi]^(-4)*couplingConstants[[8]]*couplingConstants[[9]]*couplingConstants[[10]]
-7./1024.*\[Pi]^(-4)*couplingConstants[[8]]^2*couplingConstants[[10]]
-1./128.*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[9]]*couplingConstants[[10]]
-1./128.*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[8]]*couplingConstants[[10]]
-9./512.*\[Pi]^(-4)*couplingConstants[[7]]^4*couplingConstants[[10]]
-3./128.*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[9]]*couplingConstants[[10]]
-3./128.*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[8]]*couplingConstants[[10]]
-27./512.*\[Pi]^(-4)*couplingConstants[[6]]^4*couplingConstants[[10]]
-3./128.*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[9]]*couplingConstants[[10]]
-3./128.*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[8]]*couplingConstants[[10]]
-27./512.*\[Pi]^(-4)*couplingConstants[[5]]^4*couplingConstants[[10]]
-3./128.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[9]]*couplingConstants[[10]]
-3./128.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[8]]*couplingConstants[[10]]
+27./256.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]^2*couplingConstants[[10]]
-27./512.*\[Pi]^(-4)*couplingConstants[[4]]^4*couplingConstants[[10]]
+5./32.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[6]]^2*couplingConstants[[10]]
+5./32.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]^2*couplingConstants[[10]]
+5./32.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]^2*couplingConstants[[10]]
+15./512.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[9]]*couplingConstants[[10]]
+15./1024.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[7]]^2*couplingConstants[[10]]
+45./1024.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[6]]^2*couplingConstants[[10]]
+45./1024.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]^2*couplingConstants[[10]]
+45./1024.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]^2*couplingConstants[[10]]
-217./4096.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[10]]
+1./8.*\[Pi]^(-2)*couplingConstants[[9]]*couplingConstants[[10]]
+1./16.*\[Pi]^(-2)*couplingConstants[[8]]*couplingConstants[[10]]
+1./8.*\[Pi]^(-2)*couplingConstants[[7]]^2*couplingConstants[[10]]
+3./8.*\[Pi]^(-2)*couplingConstants[[6]]^2*couplingConstants[[10]]
+3./8.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[10]]
+3./8.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[10]]
-9./32.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[10]]
+1./512.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[9]]*couplingConstants[[10]]
+1./128.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[8]]*couplingConstants[[10]]
+25./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[10]]
+85./3072.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[10]]
+25./3072.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[10]]
+85./3072.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[10]]
+47./2048.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[10]]
+3./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph*couplingConstants[[10]]^2
+1./64.*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph*couplingConstants[[2]]^2*couplingConstants[[9]]
+5./64.*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph*couplingConstants[[2]]^2*couplingConstants[[8]]
+11./32.*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph*couplingConstants[[2]]^2*couplingConstants[[7]]^2
+21./32.*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph*couplingConstants[[2]]^2*couplingConstants[[6]]^2
+9./32.*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph*couplingConstants[[2]]^2*couplingConstants[[5]]^2
+21./32.*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph*couplingConstants[[2]]^2*couplingConstants[[4]]^2
-19./384.*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph*couplingConstants[[2]]^4
+1./512.*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[9]]*couplingConstants[[10]]
-1./256.*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2*couplingConstants[[10]]
-3./32.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[10]]
+3./2.*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph*couplingConstants[[2]]^2
-3./32.*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2*couplingConstants[[10]]
+437./12288.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[10]]
-379./384.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph*couplingConstants[[2]]^2
+223./6144.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[10]]
-15./128.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^3*couplingConstants[[2]]^2
+5./4096.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4*couplingConstants[[10]]
-1./4096.*multipletDim[jphi]*\[Pi]^(-4)*couplingConstants[[9]]^2*couplingConstants[[10]]
+11./12288.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[10]]
-7./384.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^3*couplingConstants[[2]]^2
+11./12288.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4*couplingConstants[[10]]
+5./49152.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[10]]^3
+11./3072.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[10]]
-7./288.*multipletDim[jphi]*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph*couplingConstants[[2]]^4
+5./384.*multipletDim[jphi]*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2*couplingConstants[[10]]
+11./2304.*multipletDim[jphi]*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[10]]
-5./8192.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[10]]^3
+1./128.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[9]]*couplingConstants[[10]]
-167./1536.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[10]]
-3./8.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[10]]
-15./32.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph*couplingConstants[[2]]^4
+5./512.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2*couplingConstants[[10]]
+5./256.*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[10]]
+Sum[-1./16./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[jphi]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[10]]
+1./32./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[10]]
+1./128./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[9]]*couplingConstants[[10]]
+3./64./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[10]]
-5./32./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph*couplingConstants[[2]]^2
-1./64./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[10]]
+3./512./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]^2*\[Pi]^(-4)*couplingConstants[[10]]
-1./128./(multipletDim[jphi]*multipletJiso[jphi])*multipletDim[J1]*multipletJiso[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[9]]*couplingConstants[[10]]
-3./128./(multipletDim[jphi]*multipletJiso[jphi])*multipletDim[J1]*multipletJiso[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[10]]
+5./64./(multipletDim[jphi]*multipletJiso[jphi])*multipletDim[J1]*multipletJiso[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph*couplingConstants[[2]]^2
+1./128./(multipletDim[jphi]*multipletJiso[jphi])*multipletDim[J1]*multipletJiso[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[10]]
-1./256./(multipletDim[jphi]*multipletJiso[jphi])*multipletDim[J1]*multipletJiso[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]^2*\[Pi]^(-4)*couplingConstants[[10]]
+1./8./(multipletDim[jphi]*multipletJiso[jphi])*multipletDim[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*(1/2multipletJiso[J1]-multipletJiso[jphi])*\[Pi]^(-2)*couplingConstants[[10]],{J1,Jsymmin[jphi],2jphi,2}],
Table[(*mu couplingConstants[J]'[mu]\[Equal]*)-1./512.*\[Pi]^(-4)*couplingConstants[[9]]^3
-1./256.*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[9]]^2
-3./256.*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[9]]^2
-3./256.*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[9]]^2
-3./256.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[9]]^2
+3./256.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[9]]^2
+1./32.*\[Pi]^(-2)*couplingConstants[[9]]^2
+1./256.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[9]]^2
+5./512.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[9]]
+3./32.*couplingConstants[[1]]^4*\[Pi]^(-2)*Yph^4
-167./1536.*couplingConstants[[1]]^6*\[Pi]^(-4)*Yph^4
-15./1024.*couplingConstants[[1]]^6*\[Pi]^(-4)*Yph^6
-7./3072.*multipletDim[jphi]*couplingConstants[[1]]^6*\[Pi]^(-4)*Yph^6
+7./768.*multipletDim[jphi]*multipletJiso[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4*couplingConstants[[2]]^2
+7./288.*multipletDim[jphi]*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^6
+7./576.*multipletDim[jphi]*multipletJiso[jphi]^2*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^4
-7./144.*multipletDim[jphi]*multipletJiso[jphi]^3*\[Pi]^(-4)*couplingConstants[[2]]^6
+11./1152.*multipletDim[jphi]*multipletJiso[jphi]^2*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
-7./1536.*multipletDim[jphi]*multipletJiso[J]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4*couplingConstants[[2]]^2
-7./576.*multipletDim[jphi]*multipletJiso[J]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^6
-7./1152.*multipletDim[jphi]*multipletJiso[J]*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^4
+7./144.*multipletDim[jphi]*multipletJiso[J]*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^6
-7./576.*multipletDim[jphi]*multipletJiso[J]^2*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^6
+11./6144.*multipletDim[jphi]*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4
+1./2048.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[9]]*couplingConstants[[10]]^2
+1./1024.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[10]]^2
+3./1024.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[10]]^2
+3./1024.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[10]]^2
+3./1024.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[10]]^2
+5./128.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[9]]
+23./192.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^6
-1./128.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[10]]^2
-3./4.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^4
-1./1024.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[10]]^2
-5./256.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph*couplingConstants[[2]]^2*couplingConstants[[10]]
-13./384.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^4
-3./4.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2*couplingConstants[[2]]^2
+167./384.*multipletJiso[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
+15./256.*multipletJiso[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4*couplingConstants[[2]]^2
+103./96.*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^6
+3./2.*multipletJiso[jphi]^2*\[Pi]^(-2)*couplingConstants[[2]]^4
+15./64.*multipletJiso[jphi]^2*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^4
-15./16.*multipletJiso[jphi]^3*\[Pi]^(-4)*couplingConstants[[2]]^6
+9./128.*multipletJiso[jphi]^2*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
+3./4096.*multipletJiso[jphi]*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[10]]^2
-155./768.*multipletJiso[jphi]*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
-3./4.*multipletJiso[jphi]*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-2)*couplingConstants[[2]]^2
+1./256.*multipletJiso[jphi]*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
+1./32.*multipletJiso[jphi]*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2
-Sum[1./16.*multipletJiso[jphi]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[11+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*\[Pi]^(-4)*couplingConstants[[2]]^2,{J1,Jsymmin[jphi],2jphi,2},{J2,Jsymmin[jphi],2jphi,2}]
-1./2048.*multipletJiso[J]*\[Pi]^(-4)*couplingConstants[[9]]*couplingConstants[[10]]^2
-1./2048.*multipletJiso[J]*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[10]]^2
-3./2048.*multipletJiso[J]*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[10]]^2
-3./2048.*multipletJiso[J]*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[10]]^2
-3./2048.*multipletJiso[J]*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[10]]^2
-23./384.*multipletJiso[J]*\[Pi]^(-4)*couplingConstants[[2]]^6
+1./256.*multipletJiso[J]*\[Pi]^(-2)*couplingConstants[[10]]^2
+3./8.*multipletJiso[J]*\[Pi]^(-2)*couplingConstants[[2]]^4
+1./2048.*multipletJiso[J]*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[10]]^2
+5./512.*multipletJiso[J]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph*couplingConstants[[2]]^2*couplingConstants[[10]]
+13./768.*multipletJiso[J]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^4
+3./8.*multipletJiso[J]*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2*couplingConstants[[2]]^2
-167./768.*multipletJiso[J]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
-15./256.*multipletJiso[J]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4*couplingConstants[[2]]^2
+1./192.*multipletJiso[J]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^6
-3./2.*multipletJiso[J]*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^4
+15./16.*multipletJiso[J]*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^6
-1./64.*multipletJiso[J]*multipletJiso[jphi]*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
-23./384.*multipletJiso[J]^2*\[Pi]^(-4)*couplingConstants[[2]]^6
+3./8.*multipletJiso[J]^2*\[Pi]^(-2)*couplingConstants[[2]]^4
-15./256.*multipletJiso[J]^2*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^4
-15./64.*multipletJiso[J]^2*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^6
+1./256.*multipletJiso[J]^2*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
-1./2048.*multipletJiso[J]*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[10]]^2
+1./128.*multipletJiso[J]*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
+1./128.*multipletJiso[J]*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
-3./256.*multipletJiso[J]*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2
+Sum[-5./16.*multipletJiso[J1]*multipletJiso[jphi]*couplingConstants[[11+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*\[Pi]^(-4)*couplingConstants[[2]]^4
-1./32.*multipletJiso[J1]*multipletJiso[jphi]*couplingConstants[[11+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*multipletPhase[jphi,J1]*\[Pi]^(-4)*couplingConstants[[2]]^4
+5./64.*multipletJiso[J1]^2*couplingConstants[[11+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*\[Pi]^(-4)*couplingConstants[[2]]^4
+1./128.*multipletJiso[J1]^2*couplingConstants[[11+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*multipletPhase[jphi,J1]*\[Pi]^(-4)*couplingConstants[[2]]^4,{J1,0,2jphi},{J2,Jsymmin[jphi],2jphi,2}]
+Sum[3./64.*multipletJiso[J1]*couplingConstants[[11+1/2 (J2-Jsymmin[jphi])]]*couplingConstants[[11+1/2 (J3-Jsymmin[jphi])]]*kCoefficient[jphi,J2,J3,J4]*kCoefficient[jphi,J4,J1,J]*multipletPhase[jphi,J1]*\[Pi]^(-4)*couplingConstants[[2]]^2,{J1,0,2jphi},{J4,0,2jphi},{J2,Jsymmin[jphi],2jphi,2},{J3,Jsymmin[jphi],2jphi,2}]
-5./1024.*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[9]]^2
-3./16.*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2
+211./3072.*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2
+9./2048.*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4
+1./16.*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]^2*\[Pi]^(-2)
-1./256.*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]^2*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2
+Sum[-1./64.*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[11+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*\[Pi]^(-4)
-1./64.*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]^2*couplingConstants[[11+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*\[Pi]^(-4),{J1,Jsymmin[jphi],2jphi,2},{J2,Jsymmin[jphi],2jphi,2}]
-Sum[1./32.*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[11+1/2 (J2-Jsymmin[jphi])]]*couplingConstants[[11+1/2 (J3-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J4]*kCoefficient[jphi,J4,J3,J]*multipletPhase[jphi,J4]*\[Pi]^(-4),{J1,Jsymmin[jphi],2jphi,2},{J2,Jsymmin[jphi],2jphi,2},{J3,Jsymmin[jphi],2jphi,2},{J4,0,2jphi}]
+Sum[1./4.*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[11+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*\[Pi]^(-2)
+1./32.*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[11+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2,{J1,Jsymmin[jphi],2jphi,2},{J2,Jsymmin[jphi],2jphi,2}]
+Sum[5./16./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[jphi]^2*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
+5./32./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[jphi]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
+5./32./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[jphi]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
-5./64./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[J]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
-5./64./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[J]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
-5./64./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
-5./64./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
+1./256./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[11+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]^2*\[Pi]^(-4)
+5./256./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4
+5./128./(multipletDim[jphi]*multipletJiso[jphi])*multipletDim[J1]*multipletJiso[J]*multipletJiso[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
+5./128./(multipletDim[jphi]*multipletJiso[jphi])*multipletDim[J1]*multipletJiso[J]*multipletJiso[J1]*couplingConstants[[11+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2,{J1,Jsymmin[jphi],2jphi,2}],{J,Jsymmin[jphi],2jphi,2}]}]


(* ::Title::Closed:: *)
(*2loop \[Beta]-Functions: SM + Complex Singlet Scalar, Hypercharge Y*)


BetaSingletEquations2l[jphi_,Yph_,couplingConstants_]:=Flatten[{
(*mu g1Coupling'[mu]\[Equal]*)-5./512.*couplingConstants[[1]]^3*\[Pi]^(-4)*couplingConstants[[7]]^2
-17./1536.*couplingConstants[[1]]^3*\[Pi]^(-4)*couplingConstants[[6]]^2
-5./1536.*couplingConstants[[1]]^3*\[Pi]^(-4)*couplingConstants[[5]]^2
-17./1536.*couplingConstants[[1]]^3*\[Pi]^(-4)*couplingConstants[[4]]^2
+11./192.*couplingConstants[[1]]^3*\[Pi]^(-4)*couplingConstants[[3]]^2
+9./512.*couplingConstants[[1]]^3*\[Pi]^(-4)*couplingConstants[[2]]^2
+41./96.*couplingConstants[[1]]^3*\[Pi]^(-2)
+199./4608.*couplingConstants[[1]]^5*\[Pi]^(-4)
+1./192.*multipletDim[jphi]*couplingConstants[[1]]^3*\[Pi]^(-2)*Yph^2
+1/(16*\[Pi]^2)^2*(1./4.*multipletDim[jphi]*couplingConstants[[1]]^5*Yph^4
+multipletDim[jphi]*multipletJiso[jphi]*couplingConstants[[1]]^3*Yph^2*couplingConstants[[2]]^2),
(*mu g2Coupling'[mu]\[Equal]*)-1./512.*\[Pi]^(-4)*couplingConstants[[2]]^3*couplingConstants[[7]]^2
-3./512.*\[Pi]^(-4)*couplingConstants[[2]]^3*couplingConstants[[6]]^2
-3./512.*\[Pi]^(-4)*couplingConstants[[2]]^3*couplingConstants[[5]]^2
-3./512.*\[Pi]^(-4)*couplingConstants[[2]]^3*couplingConstants[[4]]^2
+3./64.*\[Pi]^(-4)*couplingConstants[[2]]^3*couplingConstants[[3]]^2
+35./1536.*\[Pi]^(-4)*couplingConstants[[2]]^5
-19./96.*\[Pi]^(-2)*couplingConstants[[2]]^3
+3./512.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^3
+1./144.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^3
+1/(16*\[Pi]^2)^2*(4./9.*multipletDim[jphi]*multipletJiso[jphi]*couplingConstants[[2]]^5
+1./3.*multipletDim[jphi]*multipletJiso[jphi]*couplingConstants[[1]]^2*Yph^2*couplingConstants[[2]]^3
+4./3.*multipletDim[jphi]*multipletJiso[jphi]^2*couplingConstants[[2]]^5),
(*mu gs'[mu]\[Equal]*)-1./128.*\[Pi]^(-4)*couplingConstants[[3]]^3*couplingConstants[[6]]^2
-1./128.*\[Pi]^(-4)*couplingConstants[[3]]^3*couplingConstants[[5]]^2
-1./128.*\[Pi]^(-4)*couplingConstants[[3]]^3*couplingConstants[[4]]^2
-13./128.*\[Pi]^(-4)*couplingConstants[[3]]^5
+9./512.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[3]]^3
-7./16.*\[Pi]^(-2)*couplingConstants[[3]]^3
+11./1536.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[3]]^3,
(*mu ytCouplingop'[mu]\[Equal]*)3./2048.*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[8]]^2
-9./1024.*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[7]]^4
-27./1024.*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[6]]^4
+5./1024.*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[5]]^2*couplingConstants[[7]]^2
+15./1024.*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[5]]^2*couplingConstants[[6]]^2
-1./1024.*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[5]]^4
-3./256.*\[Pi]^(-4)*couplingConstants[[4]]^3*couplingConstants[[8]]
-9./1024.*\[Pi]^(-4)*couplingConstants[[4]]^3*couplingConstants[[7]]^2
-27./1024.*\[Pi]^(-4)*couplingConstants[[4]]^3*couplingConstants[[6]]^2
-11./1024.*\[Pi]^(-4)*couplingConstants[[4]]^3*couplingConstants[[5]]^2
-3./64.*\[Pi]^(-4)*couplingConstants[[4]]^5
+5./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]*couplingConstants[[6]]^2
+1./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]*couplingConstants[[5]]^2
+9./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]^3
-27./64.*\[Pi]^(-4)*couplingConstants[[3]]^4*couplingConstants[[4]]
+15./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]*couplingConstants[[7]]^2
+45./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]*couplingConstants[[6]]^2
+99./4096.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]*couplingConstants[[5]]^2
+225./4096.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]^3
+9./256.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[3]]^2*couplingConstants[[4]]
-23./1024.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[4]]
+1./16.*\[Pi]^(-2)*couplingConstants[[4]]*couplingConstants[[7]]^2
+3./16.*\[Pi]^(-2)*couplingConstants[[4]]*couplingConstants[[6]]^2
+3./32.*\[Pi]^(-2)*couplingConstants[[4]]*couplingConstants[[5]]^2
+9./32.*\[Pi]^(-2)*couplingConstants[[4]]^3
-1./2.*\[Pi]^(-2)*couplingConstants[[3]]^2*couplingConstants[[4]]
-9./64.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[4]]
+25./2048.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[7]]^2
+85./6144.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[6]]^2
+7./12288.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[5]]^2
+131./4096.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]^3
+19./2304.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]
-3./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]
-17./192.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[4]]
+1187./55296.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[4]]
+1./8192.*multipletDim[jphi]*\[Pi]^(-4)*couplingConstants[[4]]*couplingConstants[[9]]^2
+5./6912.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[4]]
+1./768.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[4]],
(*mu ybCouplingot'[mu]\[Equal]*)3./2048.*\[Pi]^(-4)*couplingConstants[[5]]*couplingConstants[[8]]^2
-9./1024.*\[Pi]^(-4)*couplingConstants[[5]]*couplingConstants[[7]]^4
-27./1024.*\[Pi]^(-4)*couplingConstants[[5]]*couplingConstants[[6]]^4
-3./256.*\[Pi]^(-4)*couplingConstants[[5]]^3*couplingConstants[[8]]
-9./1024.*\[Pi]^(-4)*couplingConstants[[5]]^3*couplingConstants[[7]]^2
-27./1024.*\[Pi]^(-4)*couplingConstants[[5]]^3*couplingConstants[[6]]^2
-3./64.*\[Pi]^(-4)*couplingConstants[[5]]^5
+5./1024.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]*couplingConstants[[7]]^2
+15./1024.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]*couplingConstants[[6]]^2
-11./1024.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]^3
-1./1024.*\[Pi]^(-4)*couplingConstants[[4]]^4*couplingConstants[[5]]
+5./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]*couplingConstants[[6]]^2
+9./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]^3
+1./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]^2*couplingConstants[[5]]
-27./64.*\[Pi]^(-4)*couplingConstants[[3]]^4*couplingConstants[[5]]
+15./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]*couplingConstants[[7]]^2
+45./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]*couplingConstants[[6]]^2
+225./4096.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]^3
+99./4096.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]^2*couplingConstants[[5]]
+9./256.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[3]]^2*couplingConstants[[5]]
-23./1024.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[5]]
+1./16.*\[Pi]^(-2)*couplingConstants[[5]]*couplingConstants[[7]]^2
+3./16.*\[Pi]^(-2)*couplingConstants[[5]]*couplingConstants[[6]]^2
+9./32.*\[Pi]^(-2)*couplingConstants[[5]]^3
+3./32.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[5]]
-1./2.*\[Pi]^(-2)*couplingConstants[[3]]^2*couplingConstants[[5]]
-9./64.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[5]]
+25./2048.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]*couplingConstants[[7]]^2
+85./6144.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]*couplingConstants[[6]]^2
+79./4096.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]^3
+91./12288.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]
+31./2304.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]
-9./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]
-5./192.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[5]]
-127./55296.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[5]]
+1./8192.*multipletDim[jphi]*\[Pi]^(-4)*couplingConstants[[5]]*couplingConstants[[9]]^2
+7./55296.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[5]]
+1./768.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[5]],
(*mu ycCouplingha'[mu]\[Equal]*)3./2048.*\[Pi]^(-4)*couplingConstants[[6]]*couplingConstants[[8]]^2
-9./1024.*\[Pi]^(-4)*couplingConstants[[6]]*couplingConstants[[7]]^4
-3./256.*\[Pi]^(-4)*couplingConstants[[6]]^3*couplingConstants[[8]]
-9./1024.*\[Pi]^(-4)*couplingConstants[[6]]^3*couplingConstants[[7]]^2
-3./64.*\[Pi]^(-4)*couplingConstants[[6]]^5
-27./1024.*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[6]]^3
-27./1024.*\[Pi]^(-4)*couplingConstants[[5]]^4*couplingConstants[[6]]
-27./1024.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[6]]^3
+3./512.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]^2*couplingConstants[[6]]
-27./1024.*\[Pi]^(-4)*couplingConstants[[4]]^4*couplingConstants[[6]]
+9./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[6]]^3
+5./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]^2*couplingConstants[[6]]
+5./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]^2*couplingConstants[[6]]
-27./64.*\[Pi]^(-4)*couplingConstants[[3]]^4*couplingConstants[[6]]
+15./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[6]]*couplingConstants[[7]]^2
+225./4096.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[6]]^3
+45./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]^2*couplingConstants[[6]]
+45./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]^2*couplingConstants[[6]]
+9./256.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[3]]^2*couplingConstants[[6]]
-23./1024.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[6]]
+1./16.*\[Pi]^(-2)*couplingConstants[[6]]*couplingConstants[[7]]^2
+9./32.*\[Pi]^(-2)*couplingConstants[[6]]^3
+3./16.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[6]]
+3./16.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[6]]
-1./2.*\[Pi]^(-2)*couplingConstants[[3]]^2*couplingConstants[[6]]
-9./64.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[6]]
+25./2048.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[6]]*couplingConstants[[7]]^2
+131./4096.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[6]]^3
+25./6144.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[6]]
+85./6144.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[6]]
+19./2304.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[6]]
-3./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[6]]
-17./192.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[6]]
+1187./55296.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[6]]
+1./8192.*multipletDim[jphi]*\[Pi]^(-4)*couplingConstants[[6]]*couplingConstants[[9]]^2
+5./6912.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[6]]
+1./768.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[6]],
(*mu ytCouplingau'[mu]\[Equal]*)3./2048.*\[Pi]^(-4)*couplingConstants[[7]]*couplingConstants[[8]]^2
-3./256.*\[Pi]^(-4)*couplingConstants[[7]]^3*couplingConstants[[8]]
-3./256.*\[Pi]^(-4)*couplingConstants[[7]]^5
-27./1024.*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[7]]^3
-27./1024.*\[Pi]^(-4)*couplingConstants[[6]]^4*couplingConstants[[7]]
-27./1024.*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[7]]^3
-27./1024.*\[Pi]^(-4)*couplingConstants[[5]]^4*couplingConstants[[7]]
-27./1024.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[7]]^3
+3./512.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]^2*couplingConstants[[7]]
-27./1024.*\[Pi]^(-4)*couplingConstants[[4]]^4*couplingConstants[[7]]
+5./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[6]]^2*couplingConstants[[7]]
+5./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]^2*couplingConstants[[7]]
+5./64.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]^2*couplingConstants[[7]]
+165./4096.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[7]]^3
+45./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[6]]^2*couplingConstants[[7]]
+45./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]^2*couplingConstants[[7]]
+45./2048.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]^2*couplingConstants[[7]]
-23./1024.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[7]]
+5./32.*\[Pi]^(-2)*couplingConstants[[7]]^3
+3./16.*\[Pi]^(-2)*couplingConstants[[6]]^2*couplingConstants[[7]]
+3./16.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[7]]
+3./16.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[7]]
-9./64.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[7]]
+179./4096.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[7]]^3
+85./6144.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[7]]
+25./6144.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[7]]
+85./6144.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[7]]
+9./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[7]]
-15./64.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[7]]
+457./6144.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[7]]
+1./8192.*multipletDim[jphi]*\[Pi]^(-4)*couplingConstants[[7]]*couplingConstants[[9]]^2
+13./6144.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[7]]
+1./768.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[7]],
(*mu lam'[mu]\[Equal]*)-39./512.*\[Pi]^(-4)*couplingConstants[[8]]^3
-3./64.*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[8]]^2
-1./256.*\[Pi]^(-4)*couplingConstants[[7]]^4*couplingConstants[[8]]
+5./32.*\[Pi]^(-4)*couplingConstants[[7]]^6
-9./64.*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[8]]^2
-3./256.*\[Pi]^(-4)*couplingConstants[[6]]^4*couplingConstants[[8]]
+15./32.*\[Pi]^(-4)*couplingConstants[[6]]^6
-9./64.*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[8]]^2
-3./256.*\[Pi]^(-4)*couplingConstants[[5]]^4*couplingConstants[[8]]
+15./32.*\[Pi]^(-4)*couplingConstants[[5]]^6
-9./64.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[8]]^2
-21./128.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]^2*couplingConstants[[8]]
-3./32.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]^4
-3./256.*\[Pi]^(-4)*couplingConstants[[4]]^4*couplingConstants[[8]]
-3./32.*\[Pi]^(-4)*couplingConstants[[4]]^4*couplingConstants[[5]]^2
+15./32.*\[Pi]^(-4)*couplingConstants[[4]]^6
+5./16.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[6]]^2*couplingConstants[[8]]
-1./2.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[6]]^4
+5./16.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]^2*couplingConstants[[8]]
-1./2.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]^4
+5./16.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]^2*couplingConstants[[8]]
-1./2.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]^4
+27./256.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[8]]^2
+15./512.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[7]]^2*couplingConstants[[8]]
+45./512.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[6]]^2*couplingConstants[[8]]
+45./512.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]^2*couplingConstants[[8]]
+45./512.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]^2*couplingConstants[[8]]
-73./2048.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[8]]
-3./256.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[7]]^2
-9./256.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[6]]^2
-9./256.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[5]]^2
-9./256.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[4]]^2
+305./1024.*\[Pi]^(-4)*couplingConstants[[2]]^6
+3./8.*\[Pi]^(-2)*couplingConstants[[8]]^2
+1./4.*\[Pi]^(-2)*couplingConstants[[7]]^2*couplingConstants[[8]]
-1./2.*\[Pi]^(-2)*couplingConstants[[7]]^4
+3./4.*\[Pi]^(-2)*couplingConstants[[6]]^2*couplingConstants[[8]]
-3./2.*\[Pi]^(-2)*couplingConstants[[6]]^4
+3./4.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[8]]
-3./2.*\[Pi]^(-2)*couplingConstants[[5]]^4
+3./4.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[8]]
-3./2.*\[Pi]^(-2)*couplingConstants[[4]]^4
-9./16.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[8]]
+9./32.*\[Pi]^(-2)*couplingConstants[[2]]^4
+9./256.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[8]]^2
+25./512.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[8]]
-1./16.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[7]]^4
+85./1536.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[8]]
-1./24.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[6]]^4
+25./1536.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[8]]
+1./48.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]^4
+85./1536.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[8]]
-1./24.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]^4
+39./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[8]]
+11./128.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[7]]^2
+21./128.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[6]]^2
+9./128.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]^2
+21./128.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]^2
-289./3072.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^4
-3./16.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[8]]
+3./16.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[2]]^2
+629./6144.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[8]]
-25./256.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[7]]^2
-19./256.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[6]]^2
+5./256.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[5]]^2
-19./256.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[4]]^2
-559./3072.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[2]]^2
+3./32.*couplingConstants[[1]]^4*\[Pi]^(-2)
-379./3072.*couplingConstants[[1]]^6*\[Pi]^(-4)
-1./1024.*multipletDim[jphi]*\[Pi]^(-4)*couplingConstants[[9]]^3
-5./2048.*multipletDim[jphi]*\[Pi]^(-4)*couplingConstants[[8]]*couplingConstants[[9]]^2
+1./64.*multipletDim[jphi]*\[Pi]^(-2)*couplingConstants[[9]]^2
+1./512.*multipletDim[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[9]]^2
+5./1024.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[9]]
+11./6144.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[8]]
-7./3072.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
-7./3072.*multipletDim[jphi]*couplingConstants[[1]]^6*\[Pi]^(-4)*Yph^2
+1./128.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[9]]^2
+5./256.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[9]]
+11./1536.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[8]]
-7./768.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^6
-7./2304.*multipletDim[jphi]*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^4,
(*mu l1phH'[mu]\[Equal]*)-5./2048.*\[Pi]^(-4)*couplingConstants[[9]]^3
-9./512.*\[Pi]^(-4)*couplingConstants[[8]]*couplingConstants[[9]]^2
-15./1024.*\[Pi]^(-4)*couplingConstants[[8]]^2*couplingConstants[[9]]
-1./256.*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[9]]^2
-3./128.*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[8]]*couplingConstants[[9]]
-9./512.*\[Pi]^(-4)*couplingConstants[[7]]^4*couplingConstants[[9]]
-3./256.*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[9]]^2
-9./128.*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[8]]*couplingConstants[[9]]
-27./512.*\[Pi]^(-4)*couplingConstants[[6]]^4*couplingConstants[[9]]
-3./256.*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[9]]^2
-9./128.*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[8]]*couplingConstants[[9]]
-27./512.*\[Pi]^(-4)*couplingConstants[[5]]^4*couplingConstants[[9]]
-3./256.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[9]]^2
-9./128.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[8]]*couplingConstants[[9]]
-21./256.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[5]]^2*couplingConstants[[9]]
-27./512.*\[Pi]^(-4)*couplingConstants[[4]]^4*couplingConstants[[9]]
+5./32.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[6]]^2*couplingConstants[[9]]
+5./32.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[5]]^2*couplingConstants[[9]]
+5./32.*\[Pi]^(-4)*couplingConstants[[3]]^2*couplingConstants[[4]]^2*couplingConstants[[9]]
+3./1024.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[9]]^2
+9./128.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[8]]*couplingConstants[[9]]
+15./1024.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[7]]^2*couplingConstants[[9]]
+45./1024.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[6]]^2*couplingConstants[[9]]
+45./1024.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[5]]^2*couplingConstants[[9]]
+45./1024.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[4]]^2*couplingConstants[[9]]
-145./4096.*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[9]]
+1./16.*\[Pi]^(-2)*couplingConstants[[9]]^2
+3./16.*\[Pi]^(-2)*couplingConstants[[8]]*couplingConstants[[9]]
+1./8.*\[Pi]^(-2)*couplingConstants[[7]]^2*couplingConstants[[9]]
+3./8.*\[Pi]^(-2)*couplingConstants[[6]]^2*couplingConstants[[9]]
+3./8.*\[Pi]^(-2)*couplingConstants[[5]]^2*couplingConstants[[9]]
+3./8.*\[Pi]^(-2)*couplingConstants[[4]]^2*couplingConstants[[9]]
-9./32.*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[9]]
+1./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[9]]^2
+3./128.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[8]]*couplingConstants[[9]]
+25./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[9]]
+85./3072.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[9]]
+25./3072.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[9]]
+85./3072.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[9]]
+15./2048.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[9]]
+1./1024.*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[9]]^2
-3./32.*couplingConstants[[1]]^2*\[Pi]^(-2)*couplingConstants[[9]]
-3./32.*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2*couplingConstants[[9]]
+557./12288.*couplingConstants[[1]]^4*\[Pi]^(-4)*couplingConstants[[9]]
+223./6144.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[9]]
+15./512.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[8]]
-25./256.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[7]]^2
-19./256.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[6]]^2
+5./256.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[5]]^2
-19./256.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[4]]^2
-45./1024.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
+5./4096.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4*couplingConstants[[9]]
+3./16.*couplingConstants[[1]]^4*\[Pi]^(-2)*Yph^2
-713./3072.*couplingConstants[[1]]^6*\[Pi]^(-4)*Yph^2
-15./1024.*couplingConstants[[1]]^6*\[Pi]^(-4)*Yph^4
-1./4096.*multipletDim[jphi]*\[Pi]^(-4)*couplingConstants[[9]]^3
+11./12288.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[9]]
+71./12288.*multipletDim[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4*couplingConstants[[9]]
-7./1536.*multipletDim[jphi]*couplingConstants[[1]]^6*\[Pi]^(-4)*Yph^4
+11./3072.*multipletDim[jphi]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[9]]
+71./2304.*multipletDim[jphi]*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[9]]
-7./288.*multipletDim[jphi]*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^6
+1./256.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[9]]^2
-143./1536.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[9]]
+15./128.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[8]]
-1./64.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[7]]^2
-3./64.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[6]]^2
-3./64.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[5]]^2
-3./64.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[4]]^2
+745./768.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^6
-3./8.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^2*couplingConstants[[9]]
+3./4.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^4
-15./256.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[2]]^4
+5./512.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2*couplingConstants[[9]]
-15./256.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^4
-15./256.*multipletJiso[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
+5./256.*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[9]]
-15./64.*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^6
+Sum[1./16./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[jphi]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[9]]
+5./64./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[jphi]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
-3./256./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[9]]^2
+1./8./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-2)*couplingConstants[[9]]
+1./64./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[9]]
+5./256./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2
-5./512./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]^2*\[Pi]^(-4)*couplingConstants[[9]],{J1,Jsymmin[jphi],2jphi,2}],
Table[(*mu couplingConstants[J]'[mu]\[Equal]*)-1./512.*\[Pi]^(-4)*couplingConstants[[9]]^3
-1./256.*\[Pi]^(-4)*couplingConstants[[7]]^2*couplingConstants[[9]]^2
-3./256.*\[Pi]^(-4)*couplingConstants[[6]]^2*couplingConstants[[9]]^2
-3./256.*\[Pi]^(-4)*couplingConstants[[5]]^2*couplingConstants[[9]]^2
-3./256.*\[Pi]^(-4)*couplingConstants[[4]]^2*couplingConstants[[9]]^2
+3./256.*\[Pi]^(-4)*couplingConstants[[2]]^2*couplingConstants[[9]]^2
+1./32.*\[Pi]^(-2)*couplingConstants[[9]]^2
+1./256.*couplingConstants[[1]]^2*\[Pi]^(-4)*couplingConstants[[9]]^2
+5./512.*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[9]]
+3./32.*couplingConstants[[1]]^4*\[Pi]^(-2)*Yph^4
-167./1536.*couplingConstants[[1]]^6*\[Pi]^(-4)*Yph^4
-15./1024.*couplingConstants[[1]]^6*\[Pi]^(-4)*Yph^6
-7./3072.*multipletDim[jphi]*couplingConstants[[1]]^6*\[Pi]^(-4)*Yph^6
+7./768.*multipletDim[jphi]*multipletJiso[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4*couplingConstants[[2]]^2
+7./288.*multipletDim[jphi]*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^6
+7./576.*multipletDim[jphi]*multipletJiso[jphi]^2*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^4
-7./144.*multipletDim[jphi]*multipletJiso[jphi]^3*\[Pi]^(-4)*couplingConstants[[2]]^6
+11./1152.*multipletDim[jphi]*multipletJiso[jphi]^2*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
-7./1536.*multipletDim[jphi]*multipletJiso[J]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4*couplingConstants[[2]]^2
-7./576.*multipletDim[jphi]*multipletJiso[J]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^6
-7./1152.*multipletDim[jphi]*multipletJiso[J]*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^4
+7./144.*multipletDim[jphi]*multipletJiso[J]*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^6
-7./576.*multipletDim[jphi]*multipletJiso[J]^2*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^6
+11./6144.*multipletDim[jphi]*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4
+5./128.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^4*couplingConstants[[9]]
+23./192.*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^6
-3./4.*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^4
-13./384.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^4
-3./4.*multipletJiso[jphi]*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2*couplingConstants[[2]]^2
+167./384.*multipletJiso[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
+15./256.*multipletJiso[jphi]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4*couplingConstants[[2]]^2
+103./96.*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^6
+3./2.*multipletJiso[jphi]^2*\[Pi]^(-2)*couplingConstants[[2]]^4
+15./64.*multipletJiso[jphi]^2*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^4
-15./16.*multipletJiso[jphi]^3*\[Pi]^(-4)*couplingConstants[[2]]^6
+9./128.*multipletJiso[jphi]^2*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
-155./768.*multipletJiso[jphi]*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
-3./4.*multipletJiso[jphi]*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-2)*couplingConstants[[2]]^2
+1./256.*multipletJiso[jphi]*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
+1./32.*multipletJiso[jphi]*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2
-Sum[1./16.*multipletJiso[jphi]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[10+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*\[Pi]^(-4)*couplingConstants[[2]]^2,{J1,Jsymmin[jphi],2jphi,2},{J2,Jsymmin[jphi],2jphi,2}]
-23./384.*multipletJiso[J]*\[Pi]^(-4)*couplingConstants[[2]]^6
+3./8.*multipletJiso[J]*\[Pi]^(-2)*couplingConstants[[2]]^4
+13./768.*multipletJiso[J]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^4
+3./8.*multipletJiso[J]*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2*couplingConstants[[2]]^2
-167./768.*multipletJiso[J]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
-15./256.*multipletJiso[J]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4*couplingConstants[[2]]^2
+1./192.*multipletJiso[J]*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^6
-3./2.*multipletJiso[J]*multipletJiso[jphi]*\[Pi]^(-2)*couplingConstants[[2]]^4
+15./16.*multipletJiso[J]*multipletJiso[jphi]^2*\[Pi]^(-4)*couplingConstants[[2]]^6
-1./64.*multipletJiso[J]*multipletJiso[jphi]*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
-23./384.*multipletJiso[J]^2*\[Pi]^(-4)*couplingConstants[[2]]^6
+3./8.*multipletJiso[J]^2*\[Pi]^(-2)*couplingConstants[[2]]^4
-15./256.*multipletJiso[J]^2*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^4
-15./64.*multipletJiso[J]^2*multipletJiso[jphi]*\[Pi]^(-4)*couplingConstants[[2]]^6
+1./256.*multipletJiso[J]^2*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
+1./128.*multipletJiso[J]*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
+1./128.*multipletJiso[J]*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
-3./256.*multipletJiso[J]*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]^2*\[Pi]^(-4)*couplingConstants[[2]]^2
+Sum[-5./16.*multipletJiso[J1]*multipletJiso[jphi]*couplingConstants[[10+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*\[Pi]^(-4)*couplingConstants[[2]]^4
-1./32.*multipletJiso[J1]*multipletJiso[jphi]*couplingConstants[[10+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*multipletPhase[jphi,J1]*\[Pi]^(-4)*couplingConstants[[2]]^4
+5./64.*multipletJiso[J1]^2*couplingConstants[[10+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*\[Pi]^(-4)*couplingConstants[[2]]^4
+1./128.*multipletJiso[J1]^2*couplingConstants[[10+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*multipletPhase[jphi,J1]*\[Pi]^(-4)*couplingConstants[[2]]^4,{J1,0,2jphi},{J2,Jsymmin[jphi],2jphi,2}]
+Sum[3./64.*multipletJiso[J1]*couplingConstants[[10+1/2 (J2-Jsymmin[jphi])]]*couplingConstants[[10+1/2 (J3-Jsymmin[jphi])]]*kCoefficient[jphi,J2,J3,J4]*kCoefficient[jphi,J4,J1,J]*multipletPhase[jphi,J1]*\[Pi]^(-4)*couplingConstants[[2]]^2,{J1,0,2jphi},{J4,0,2jphi},{J2,Jsymmin[jphi],2jphi,2},{J3,Jsymmin[jphi],2jphi,2}]
-5./1024.*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[9]]^2
-3./16.*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-2)*Yph^2
+211./3072.*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^2
+9./2048.*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4
+1./16.*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]^2*\[Pi]^(-2)
-1./256.*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]^2*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2
+Sum[-1./64.*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[10+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*\[Pi]^(-4)
-1./64.*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]^2*couplingConstants[[10+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*\[Pi]^(-4),{J1,Jsymmin[jphi],2jphi,2},{J2,Jsymmin[jphi],2jphi,2}]
-Sum[1./32.*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[10+1/2 (J2-Jsymmin[jphi])]]*couplingConstants[[10+1/2 (J3-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J4]*kCoefficient[jphi,J4,J3,J]*multipletPhase[jphi,J4]*\[Pi]^(-4),{J1,Jsymmin[jphi],2jphi,2},{J2,Jsymmin[jphi],2jphi,2},{J3,Jsymmin[jphi],2jphi,2},{J4,0,2jphi}]
+Sum[1./4.*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[10+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*\[Pi]^(-2)
+1./32.*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[10+1/2 (J2-Jsymmin[jphi])]]*kCoefficient[jphi,J1,J2,J]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2,{J1,Jsymmin[jphi],2jphi,2},{J2,Jsymmin[jphi],2jphi,2}]
+Sum[5./16./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[jphi]^2*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
+5./32./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[jphi]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
+5./32./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[jphi]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
-5./64./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[J]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
-5./64./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[J]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
-5./64./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[J1]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*\[Pi]^(-4)*couplingConstants[[2]]^4
-5./64./(multipletDim[jphi])*multipletDim[J1]*multipletJiso[J1]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^2*\[Pi]^(-4)*Yph^2*couplingConstants[[2]]^2
+1./256./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[10+1/2 (J-Jsymmin[jphi])]]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]^2*\[Pi]^(-4)
+5./256./(multipletDim[jphi])*multipletDim[J1]*couplingConstants[[10+1/2 (J1-Jsymmin[jphi])]]*couplingConstants[[1]]^4*\[Pi]^(-4)*Yph^4,{J1,Jsymmin[jphi],2jphi,2}],{J,Jsymmin[jphi],2jphi,2}]}]


(* ::Title:: *)
(*Function Definitions*)


GenerateCouplings[jphi_,mu_]:=If[jphi==0,Flatten[{g1Coupling[mu],g2Coupling[mu],gsCoupling[mu],ytCoupling[mu],ybCoupling[mu],ycCoupling[mu],y\[Tau]Coupling[mu],\[Lambda]HCoupling[mu],\[Lambda]\[CurlyPhi]H1Coupling[mu],Table[ToExpression["\[Lambda]\[CurlyPhi]"<>ToString[J]<>"Coupling"][mu],{J,Jsymmin[jphi],2jphi,2}]}],Flatten[{g1Coupling[mu],g2Coupling[mu],gsCoupling[mu],ytCoupling[mu],ybCoupling[mu],ycCoupling[mu],y\[Tau]Coupling[mu],\[Lambda]HCoupling[mu],\[Lambda]\[CurlyPhi]H1Coupling[mu],\[Lambda]\[CurlyPhi]H2Coupling[mu],Table[ToExpression["\[Lambda]\[CurlyPhi]"<>ToString[J]<>"Coupling"][mu],{J,Jsymmin[jphi],2jphi,2}]}]]
GenerateCouplingNames[jphi_]:=If[jphi==0,Flatten[{g1Coupling,g2Coupling,gsCoupling,ytCoupling,ybCoupling,ycCoupling,y\[Tau]Coupling,\[Lambda]HCoupling,\[Lambda]\[CurlyPhi]H1Coupling,Table[ToExpression["\[Lambda]\[CurlyPhi]"<>ToString[J]<>"Coupling"],{J,Jsymmin[jphi],2jphi,2}]}],Flatten[{g1Coupling,g2Coupling,gsCoupling,ytCoupling,ybCoupling,ycCoupling,y\[Tau]Coupling,\[Lambda]HCoupling,\[Lambda]\[CurlyPhi]H1Coupling,\[Lambda]\[CurlyPhi]H2Coupling,Table[ToExpression["\[Lambda]\[CurlyPhi]"<>ToString[J]<>"Coupling"],{J,Jsymmin[jphi],2jphi,2}]}]]
BetaFn1l[jphi_,Yph_,coups_]:=If[jphi==0,BetaSingletEquations1l[jphi,Yph,coups],BetaEquations1l[jphi,Yph,coups]]
BetaFn2l[jphi_,Yph_,coups_]:=If[jphi==0,BetaSingletEquations2l[jphi,Yph,coups],BetaEquations2l[jphi,Yph,coups]]


End[]


Protect @@ Names["ComplexMultiplet`*"]
Unprotect[g1Coupling,g2Coupling,gsCoupling,ytCoupling,ybCoupling,ycCoupling,y\[Tau]Coupling,\[Lambda]HCoupling,\[Lambda]\[CurlyPhi]H1Coupling,\[Lambda]\[CurlyPhi]H2Coupling]


EndPackage[]
