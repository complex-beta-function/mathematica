# ComplexMultiplet Mathematica Package

This Mathematica package is used to calculate the one- and two-loop beta functions of couplings for a model which extends the standard model (SM) by a single complex scalar multiplet (CSM) with weak isospin j and hypercharge Y.

# GenerateCouplings and GenerateCouplingNames

The GenerateCouplings[j,mu] function creates an array of scale mu-dependent SM and isospin j CSM couplings which can be used as inputs for later functions. The structure of the output is as follows: {g1[mu],g2[mu],gs[mu],yt[mu],yb[mu],yc[mu],ytau[mu],lamH[mu],Portal Couplings,CSM Quartic Couplings} where g1, g2, and gs are the SM U(1), SU(2), and SU(3) gauge couplings, yt, yb, yc, and ytau are the top quark, bottom quark, charm quark and tau lepton Yukawa couplings, lamH is the Higgs doublet quartic scalar coupling, Portal Couplings refers to the one (j=0) or two (j>0) CSM-Higgs doublet quartic interactions, and CSM Quartic Couplings refers to the floor(j+1) CSM-only quartic interactions.

GenerateCouplingNames[j] gives a similar output, but without any mu-dependences. The intention of this function is mainly for convenience and can be used in e.g. NDSolve when specifying the functions to solve for. 

# BetaFn1l and BetaFn2l

BetaFn1l[j,Y,{g}] gives the analytical expressions for the one-loop beta functions for the SM + CSM couplings. The inputs to this function are the scalar isospin j, hypercharge Y, and an array of couplings, {g} whcih can be either functions or scalars. NOTE: THE ORDERING OF THE COUPLNIG CONSTANTS IN THE ARRAY, {g}, DOES MATTER! The GenerateCouplings function was designed to create the couplings in the exact ordering (see above) that BetaFn1l and BetaFn2l require to function properly. While it is not necessary to use GenerateCouplings for the input {g}, it is highly recommended.

BetaFn2l[j,Y,{g}] is identical to BetaFn1l, with the exception that BetaFn2l outputs the two-loop beta functions.

# Usage

As an example, consider finding the one-loop running of the couplings for the standard model extended by a j=3, Y=0 complex scalar multiplet from the Z-mass to the TeV scale, given the initial conditions of the couplings at the Z-mass:

```
<<PathToFile\ComplexMultiplet`
mZ = 91.19;
jPhi = 3;
yPhi = 0;
len = Length[GenerateCouplingNames[jPhi]];
iconds = {0.357, 0.652, 1.163, 0.932, 0.0164, 0.00361, 0.0102, 0.504, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5};
initialConditions := Table[GenerateCouplings[jPhi, mZ][[n]] == iconds[[n]], {n, 1, len}]
eqns[mu_] := Table[mu D[GenerateCouplings[jPhi, mu][[n]], mu] == BetaFn1l[jPhi, yPhi, GenerateCouplings[jPhi, mu]][[n]], {n, 1, len}]
solveRunning = NDSolve[Flatten[{eqns[mu], initialConditions}], GenerateCouplingNames[jPhi], {mu, mZ, 10^4}]
```

## Citation
If you use our results, please cite us! To get the `BibTeX` entries, click on: [inspirehep query](https://inspirehep.net/search?p=arxiv:2007.13755) 

